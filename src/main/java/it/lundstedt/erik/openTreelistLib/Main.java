package it.lundstedt.erik.openTreelistLib;

import it.lundstedt.erik.openTreelistLib.tree.*;

import java.util.ArrayList;


public class Main
{
	public static void main(String[] args) {
/*
		Root root =new Root();
		Child branch=new Child("im a branch", root);
		Child todo=new Child("remember to eat",branch);
		Child branch2=new Child("im a branch", root);
		Child subBranch=new Child("im a sub branch",branch);
		Child subBranch1=new Child("im a subBranch",branch);
		Child subSubBranch=new Child("im a subSub branch",subBranch1);
		Child subSubBranch1=new Child("im a subSub Branch",subBranch1);
*/
		
		Root root = new Root();
		Folder home = new Folder("home",root);
		Folder bin = new Folder("bin",root);
		Folder dev = new Folder("dev",root);
		Folder redstone=new Folder("redstone",root);
		Node[] fs=new Node[]{root,home,bin,dev,redstone};
		
		
		//Node[] tree=new Node[]{root,branch,todo,branch2,subBranch,subBranch1,subSubBranch,subSubBranch1};
		/*
		for (int i = 0; i <fs.length ; i++) {
			System.out.println(fs[i].getName());
		}*/
		
/*
		System.out.println("*******************************************");
		Tree firstTree=new Tree(tree);
		firstTree.drawTree();
		System.out.println("*******************************************");
		firstTree.drawKids(2);
		System.out.println("*******************************************");
		firstTree.drawParents(2);
		System.out.println("*******************************************");
		System.out.println(firstTree.search("Branch"));
		System.out.println("*******************************************");
		System.out.println(firstTree.regexSearch("[a-z]+Branch"));
		System.out.println("*******************************************");
		*/
		ArrayList<Node> treeArray = new ArrayList<>();
		
		
		
		for (int i = 0; i <fs.length ; i++) {
			treeArray.add(fs[i]);
		}
		
		for (int i = 0; i <treeArray.size() ; i++)
		{
			System.out.println(treeArray.get(i).drawNode());
			
		}
		
		treeArray.add(new File("script.jsh",bin));
		
		for (int i = 0; i <treeArray.size() ; i++)
		{
			System.out.println(treeArray.get(i).drawNode());

		}
		
		
	}

}
