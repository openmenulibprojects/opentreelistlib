package it.lundstedt.erik.openTreelistLib.tree;

public class Node
{
	int indent;
	
	String name;
	String branch="├──";
	
	
	public void setIndent(int indent) {
		this.indent = indent;
	}
	
	public String getBranch() {
		return branch;
	}
	
	public void setBranch(String branch) {
		this.branch = branch;
	}
	
	public Node(int indent, String name) {
		this.indent = indent;
		this.name = name;
	}
	
	public int getIndent() {
		return indent;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	
	//example implementation. override
	public String drawNode()
	{
	for (int i = 0; i <= indent; i++) {
		System.out.print("|	 ");
	}
	System.out.println(name);
		return null;
	}
}


