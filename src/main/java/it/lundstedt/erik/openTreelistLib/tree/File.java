package it.lundstedt.erik.openTreelistLib.tree;

public class File extends Node
{
	Folder parent;
	public File(String name, Folder parentFolder) {
		super(parentFolder.indent + 1, name);
		this.parent = parentFolder;
	}
	
	@Override
	public String drawNode()
	{
		
		return parent.drawNode()+"/"+name;
	}
	
	
	
}
