package it.lundstedt.erik.openTreelistLib.tree;

public class Folder extends Node
{
	Node parent;
	public Folder(String name, Node nodeParent) {
		super(nodeParent.indent + 1, name);
		this.parent = nodeParent;
	}
	
	@Override
	public String drawNode(){
		return parent.getName()+this.name;
	}

	
	
	
	
	
}
