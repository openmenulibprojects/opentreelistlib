package it.lundstedt.erik.openTreelistLib.tree;

public class Root extends Node
{
	public Root() {
		super(0,"/");
	}
	
	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public String drawNode() {
		return name;
	}
}
