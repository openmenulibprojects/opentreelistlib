package it.lundstedt.erik.openTreelistLib.tree;

public class Child extends Node {
	Node parent;
	
	public Child(String name, Node nodeParent) {
		super(nodeParent.indent + 1, name);
		this.parent = nodeParent;
	}
	
	
	@Override
	public String drawNode() {
		
		for (int i = 1; i <= this.indent; i++) {
			System.out.print("│	 ");
		}
		System.out.print(parent.branch);
		System.out.println(name + " " + this.indent);
		
		return null;
	}

}